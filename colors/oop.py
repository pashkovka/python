from tkinter import *


class Rainbow:

    def __init__(self, title='Радуга', bg='#581CEB', geometry='600x300+1000+100'):
        self.root = Tk()
        self.root.title(title)
        self.root.geometry(geometry)
        self.root.resizable(True, True)
        self.root.config(bg=bg)

        self.label = Label(self.root, bg='#fff', fg='black', pady=4, text='Цвет', bd=2, relief='solid')
        self.label.pack(fill=X)

        self.entry = Entry(self.root, width=30, justify='center')
        self.entry.pack()

    def button(self, text_color, hex_color):
        Button(self.root, text=text_color, command=lambda text=text_color, hex=hex_color: self.set_text(hex, text),
               bg=hex_color).pack(fill=X)

    def set_text(self, hex, text):
        self.label['text'] = text
        self.entry.delete(0, END)
        self.entry.insert(0, hex)

    def loop(self):
        self.root.mainloop()


colors = {
    '#ff0000': 'Красный',
    '#ff7d00': 'Оранжевый',
    '#ffff00': 'Желтый',
    '#00ff00': 'Зеленый',
    '#007dff': 'Голубой',
    '#0000ff': 'Синий',
    '#7d00ff': 'Фиолетовый'
}

rainbow = Rainbow()

for k, v in colors.items():
    rainbow.button(v, k)

rainbow.loop()
