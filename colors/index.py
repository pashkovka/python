from tkinter import *

root = Tk()
root.title('Радуга')

root.geometry('600x300+1000+100')
root.resizable(True, True)
root.config(bg='#581CEB')


def label_text(bg, text):
    lab_hex['text'] = bg
    lab_color['text'] = text


colors = {
    '#ff0000': 'Красный',
    '#ff7d00': 'Оранжевый',
    '#ffff00': 'Желтый',
    '#00ff00': 'Зеленый',
    '#007dff': 'Голубой',
    '#0000ff': 'Синий',
    '#7d00ff': 'Фиолетовый'
}

lab_color = Label(root, bg='#fff', fg='black', pady=4, text='Цвет', bd=2, relief='solid')
lab_color.pack(fill=X)

lab_hex = Label(root, bg='#fff', fg='black', pady=4, text='Код', bd=2, relief='solid')
lab_hex.pack(fill=X)

for k, v in colors.items():
    Button(root, text=v, command=lambda text=v, hex=k: label_text(hex, text), bg=k).pack(fill=X)

root.mainloop()
